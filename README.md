############################################
#
# Bloatware-Removal Script
#
############################################
#
# Currently there is only one script (movefiles.sh)
# that removes the bloatware of 
# Sony Xperia Z2 Tablet [SGP512]
#
#
# SHELL COMMANDS TO FIND PACKAGES, IF YOU TRY TO BUILD A NEW SCRIPT 
#
# Find app locations with like this
# Command (adb):   adb shell 'pm list packages -f searchbox'
# Command (Shell): pm list packages -f searchbox
# Command-Result: package:/data/app/com.google.android.googlequicksearchbox-1.apk=com.google.android.googlequicksearchbox

#!/sbin/sh
# Create backup directory
mkdir -p /system/__REMOVED/etc
mkdir -p /system/__REMOVED/app
mkdir -p /system/__REMOVED/priv-app

##########################
# Move the bloatware apps
##########################
### The obvious bloat

# Other than /system/app/priv-app :
mv /system/recovery-from-boot.bak /system/__REMOVED/.
# Lots of bloat in etc/product
mv /system/etc/product /system/__REMOVED/etc/.
# Not much in these dirs, and these looks like low-level stuff (nativeaudiolatency, servicemenu...), better keep them :
# /system/vendor/overlay
# /system/vendor/app

# Google apps
mv /system/app/Books /system/__REMOVED/app/.
mv /system/app/Chrome /system/__REMOVED/app/.
mv /system/app/Drive /system/__REMOVED/app/.
mv /system/app/Gmail2 /system/__REMOVED/app/.
mv /system/app/Photos /system/__REMOVED/app/.
mv /system/app/PlusOne /system/__REMOVED/app/.
mv /system/app/Hangouts /system/__REMOVED/app/.
mv /system/app/Music2 /system/__REMOVED/app/.
mv /system/app/Videos /system/__REMOVED/app/.
mv /system/app/YouTube /system/__REMOVED/app/.
mv /system/app/Maps /system/__REMOVED/app/.
mv /system/app/KeepStub /system/__REMOVED/app/.
mv /system/app/NewsWeather /system/__REMOVED/app/.
mv /system/app/NewsstandStub /system/__REMOVED/app/.
mv /system/app/talkback /system/__REMOVED/app/.
mv /system/app/EditorsDocsStub /system/__REMOVED/app/.
mv /system/priv-app/Velvet /system/__REMOVED/priv-app/.
# We keep google TTS, we get logcat warnings if no tts available
# /system/app/GoogleTTS

# Sony themes/wallpapers
mv /system/app/BasicDreams /system/__REMOVED/app/.
mv /system/app/Galaxy4 /system/__REMOVED/app/.
mv /system/app/HoloSpiralWallpaper /system/__REMOVED/app/.
mv /system/app/LiveWallpapers /system/__REMOVED/app/.
mv /system/app/LiveWallpapersPicker /system/__REMOVED/app/.
mv /system/app/NoiseField /system/__REMOVED/app/.
mv /system/app/PhaseBeam /system/__REMOVED/app/.
mv /system/app/photoslideshow-release /system/__REMOVED/app/.
mv /system/app/PhotoTable /system/__REMOVED/app/.
mv /system/app/experienceflow2livewallpaper-release /system/__REMOVED/app/.
# Not exsists on xperia z2 tablet in current firmware
#/system/vendor/overlay/overlay-semcexperienceflow2-300-release.apk
mv /system/app/enchantedforest-release /system/__REMOVED/app/.


# Themes stuff (work without, but we may need to keep it to get black lockscreen wallpaper)
# No, Album should be enough to allow us to pick wallpapers
mv /system/app/skin-core-release /system/__REMOVED/app/.
mv /system/app/skin-picker-standard-release /system/__REMOVED/app/.
mv /system/app/theme-manager-release /system/__REMOVED/app/.
# We keep themes 000 (rainbow) and 006 007 008 (grey, black & white)
# No, we keep only 007 black, just in case
mv /system/app/Theme000-sw720dp-hdpi-release /system/__REMOVED/app/.
mv /system/app/Theme001-sw720dp-xhdpi-release /system/__REMOVED/app/.
mv /system/app/Theme002-sw720dp-xhdpi-release /system/__REMOVED/app/.
mv /system/app/Theme003-sw720dp-xhdpi-release /system/__REMOVED/app/.
mv /system/app/Theme004-sw720dp-xhdpi-release /system/__REMOVED/app/.
mv /system/app/Theme005-sw720dp-xhdpi-release /system/__REMOVED/app/.
mv /system/app/Theme006-sw720dp-xhdpi-release /system/__REMOVED/app/.
# /system/app/Theme007-sw720dp-xhdpi-release
mv /system/app/Theme008-sw720dp-xhdpi-release /system/__REMOVED/app/.


# Sony updates/accounts/services
mv /system/app/com.sonyericsson.xhs /system/__REMOVED/app/.
mv /system/app/com.sonymobile.xperialounge.services /system/__REMOVED/app/.
mv /system/app/Lifelog-googlePreloadLive-release-3.0.P.1.24-6292504 /system/__REMOVED/app/.
mv /system/app/GetMoreClient-release /system/__REMOVED/app/.
mv /system/priv-app/GetMore-stub-release /system/__REMOVED/priv-app/.
mv /system/priv-app/GetMore-release /system/__REMOVED/priv-app/.
# Not exsists on xperia z2 tablet in current firmware
#/system/priv-app/somc-get-to-know-it-release
mv /system/priv-app/MyXperia-release /system/__REMOVED/priv-app/.
mv /system/priv-app/RecommendationEngineExtension-release /system/__REMOVED/priv-app/.
mv /system/priv-app/UpdateCenter-release /system/__REMOVED/priv-app/.
# Sonyentrance = Whats new
mv /system/priv-app/sonyentrance2-release /system/__REMOVED/priv-app/.
mv /system/app/SyncHub-release /system/__REMOVED/app/.
mv /system/app/SemcWarrantyTime /system/__REMOVED/app/.
mv /system/app/sneiaccountmanager /system/__REMOVED/app/.
mv /system/app/support /system/__REMOVED/app/.
# Not exsists on xperia z2 tablet in current firmware
#/system/app/DemoAppChecker
#/system/priv-app/retaildemo
# Sony & parners apps
mv /system/app/PhotoWidget-release /system/__REMOVED/app/.
mv /system/app/WikipediaPlugin /system/__REMOVED/app/.
mv /system/app/YouTubeKaraokePlugin /system/__REMOVED/app/.
mv /system/app/YouTubePlugin /system/__REMOVED/app/.
mv /system/app/WorldClockWidget-release /system/__REMOVED/app/.
mv /system/app/WeatherWidget-release /system/__REMOVED/app/.
# Not exsists on xperia z2 tablet in current firmware
#/system/app/facebook-appmanager-xhdpi
#/system/priv-app/facebook-installer
mv /system/priv-app/com.mobisystems.fileman /system/__REMOVED/priv-app/.
# We keep clock widgets
# /system/app/ClockWidgets-release


##########################
### The less obvious bloat

# For Xperia companion (?) that we don't use anymore (we use Flashtool)
mv /system/priv-app/usb-mtp-backup-transport /system/__REMOVED/priv-app/.
mv /system/priv-app/usb-mtp-factoryreset-wrapper /system/__REMOVED/priv-app/.
mv /system/priv-app/usb-mtp-marlin-wrapper /system/__REMOVED/priv-app/.
mv /system/priv-app/usb-mtp-update-wrapper /system/__REMOVED/priv-app/.
mv /system/priv-app/usb-mtp-vendor-extension-service /system/__REMOVED/priv-app/.


# Sony apps/features we don't really care about
mv /system/app/TouchBlock /system/__REMOVED/app/.
mv /system/app/gmail-reader-service /system/__REMOVED/app/.
mv /system/app/GoogleLyricsPlugin /system/__REMOVED/app/.
mv /system/app/HTMLViewer /system/__REMOVED/app/.
mv /system/priv-app/TopContactsProvider /system/__REMOVED/priv-app/.
# Not exsists on xperia z2 tablet in current firmware
#/system/app/XperiaTransferMobile-release
#/system/app/cover-widget
#/system/priv-app/CoverApp
mv /system/priv-app/ActiveClipper /system/__REMOVED/priv-app/.
# We only keep SmallAppManagerService : avoid warnings in logcat (but yet no small app)
# /system/priv-app/SmallAppManagerService-release
mv /system/priv-app/SmallApp-Calculator-release /system/__REMOVED/priv-app/.
mv /system/priv-app/SmallAppsFramework-release /system/__REMOVED/priv-app/.
mv /system/priv-app/SmallAppsLauncher-release /system/__REMOVED/priv-app/.
mv /system/priv-app/SmallApp-Timer-release /system/__REMOVED/priv-app/.
mv /system/priv-app/SmallAppWidget-release /system/__REMOVED/priv-app/.
# We keep Sony Tasks, Agenda has some dependancies on it (otherwise logcat warnings)
# /system/app/Tasks

# We will reinstall updated Album from Store
mv /system/priv-app/album-albumLive-release /system/__REMOVED/priv-app/.
# To edit pictures in case we need to
# /system/priv-app/SemcPhotoEditor
# Other apps we don't care about
mv /system/priv-app/SomcPhotoAnalyzer /system/__REMOVED/priv-app/.
mv /system/app/SomcMovieCreatorRmm-release /system/__REMOVED/app/.
mv /system/priv-app/SomcMovieCreator-release /system/__REMOVED/priv-app/.
mv /system/priv-app/SemcVideo /system/__REMOVED/priv-app/.
mv /system/priv-app/SemcMusic /system/__REMOVED/priv-app/.
mv /system/priv-app/SomcPodcast /system/__REMOVED/priv-app/.
mv /system/app/SemcEmail /system/__REMOVED/app/.
# Sony Video store
mv /system/priv-app/SnpVUStore /system/__REMOVED/priv-app/.
# Music Metadata
mv /system/app/SemcMetadataCleanup /system/__REMOVED/app/.

# Camera gadget addons
mv /system/priv-app/ArtFilterCamera-hdpi-release /system/__REMOVED/priv-app/.
mv /system/app/ar-effect /system/__REMOVED/app/.
mv /system/priv-app/SemcCamera3D-hdpi-release /system/__REMOVED/priv-app/.
# Not exsists on xperia z2 tablet in current firmware
#/system/priv-app/StylePortrait
#/system/app/StyleBlue-release
#/system/app/StyleBubble-release
#/system/app/StyleDaily-release
#/system/app/StylePaint-release
#/system/app/StyleRed-release
#/system/app/StyleStar-release
#/system/app/StyleSunshine-release
#/system/app/StyleSuntan-release
mv /system/priv-app/DualCamera-hdpi-release /system/__REMOVED/priv-app/.
#/system/priv-app/FaceFusionCamera-hdpi-release
mv /system/priv-app/OnlineRemoteCamera-hdpi-release /system/__REMOVED/priv-app/.
mv /system/priv-app/SoundPhotoCamera-hdpi-release /system/__REMOVED/priv-app/.
mv /system/priv-app/CameraWearableBridgeHandheldServer /system/__REMOVED/priv-app/.

# liquid: More Camera gadget addons
mv /system/priv-app/com.sonymobile.backgrounddefocus /system/__REMOVED/priv-app/.

# liquid: Some more Sony Stuff
mv /system/priv-app/youtubelive /system/__REMOVED/priv-app/.
mv /system/app/com.sonymobile.androidapp.diagnosticslauncher /system/__REMOVED/app/.
mv /system/app/diagnostics /system/__REMOVED/app/.
mv /system/app/sociallife /system/__REMOVED/app/.


# Tv & wireless sharing
mv /system/app/dlna-somc-xhdpi-release /system/__REMOVED/app/.
mv /system/priv-app/SemcTvOut /system/__REMOVED/priv-app/.
mv /system/priv-app/SomcMirrorLinkManualSwitch /system/__REMOVED/priv-app/.
mv /system/priv-app/SomcMirrorLinkServer /system/__REMOVED/priv-app/.
mv /system/priv-app/SomcMirrorLinkSystem /system/__REMOVED/priv-app/.
mv /system/priv-app/SomcPlayAnywhere-release /system/__REMOVED/priv-app/.
# We let everything "wifi display", not really clear what is sony/qualcomm/atfWD
# and what makes Chromecast or Miracast in settings...
# (ATFWD may mean AT command forward, which would be completely something else)
# Could be added to build.prop : persist.radio.atfwd.start=false
# /system/priv-app/SomcWifiDisplay
# Qualcomm Wifi display
# /system/app/WfdService

# Gamepad, smartwatch, heartbeat & co
mv /system/app/AntHalService /system/__REMOVED/app/.
mv /system/priv-app/livewaremanager /system/__REMOVED/priv-app/.
mv /system/app/DualShockManager /system/__REMOVED/app/.
mv /system/app/RemoteControlService /system/__REMOVED/app/.

# Asian keyboards
mv /system/app/ExternalKeyboardJP /system/__REMOVED/app/.
mv /system/app/SomcPOBox /system/__REMOVED/app/.
mv /system/priv-app/textinput-chn-hdpi /system/__REMOVED/priv-app/.

# Intelligent stuff
mv /system/app/Iengine /system/__REMOVED/app/.
mv /system/app/IntelligentBacklight /system/__REMOVED/app/.
mv /system/app/IntelligentObserver /system/__REMOVED/app/.
mv /system/app/IntelligentRotation /system/__REMOVED/app/.
mv /system/app/FaceLock /system/__REMOVED/app/.

# OverTheAir stuff
mv /system/app/OmaDownload /system/__REMOVED/app/.
mv /system/app/OmaV1AgentDownloadServices /system/__REMOVED/app/.
# Not exsists on xperia z2 tablet in current firmware
#/system/app/fota-service
mv /system/priv-app/OMAClientProvisioning-release /system/__REMOVED/priv-app/.

# This app deletes some becknmark apps and block access to android forums (launched in
# some very specific context ?!)
# Not exsists on xperia z2 tablet in current firmware
#/system/app/pip

# Sony background services, crash & usage reporting
mv /system/app/AnonymousData /system/__REMOVED/app/.
mv /system/priv-app/CrashMonitor /system/__REMOVED/priv-app/.
mv /system/priv-app/CrashMonitorSystem /system/__REMOVED/priv-app/.
mv /system/app/IddAgent /system/__REMOVED/app/.
# Not exsists on xperia z2 tablet in current firmware
#/system/app/RcaHandler
mv /system/app/phone-usage /system/__REMOVED/app/.
mv /system/app/device-monitor /system/__REMOVED/app/.
mv /system/app/GoogleAnalyticsProxy /system/__REMOVED/app/.
# Setting "Storage/Move stuff to Sd Card" will Force-Close without this
# but it checks disk usage every 10 minutes ! :
mv /system/priv-app/SemcStorageChecker /system/__REMOVED/priv-app/.
# Xperia configurator/Enterprise/Cloud backup...
mv /system/app/DeviceConfigTool /system/__REMOVED/app/.

# Exchange & Enterprise stuff
mv /system/app/Exchange3Google /system/__REMOVED/app/.
mv /system/app/Exchange2 /system/__REMOVED/app/.
mv /system/app/SomcEnterpriseInstallationService /system/__REMOVED/app/.
mv /system/priv-app/EnterpriseService /system/__REMOVED/priv-app/.
mv /system/app/OneTimePassLockScreenApp /system/__REMOVED/app/.
# Not exsists on xperia z2 tablet in current firmware
#/system/priv-app/RemoteUnlockService

# DRM stuff : we keep them, may be needed for Sony own drm stuff
# /system/app/DrmDialogs
# /system/app/SecureClockService
# /system/priv-app/DtcpCtrl-release
# /system/app/KerberosService

# Works fine without all that :
# These creates some bookmarks sqlite dbs
mv /system/app/BookmarkProvider /system/__REMOVED/app/.
mv /system/app/PartnerBookmarksProvider /system/__REMOVED/app/.
mv /system/priv-app/BackupRestoreConfirmation /system/__REMOVED/priv-app/.
mv /system/priv-app/EmergencySms /system/__REMOVED/priv-app/.
# Backup on SD card
# /system/priv-app/SharedStorageBackup
# We keep this one to avoir warnings in logcat
# /system/app/PrintSpooler

# These two do not prevent the SIM card and phone from working :
# "Sim service" app : if kept, our carrier icon is recreated in Home launcher at each boot
mv /system/app/Stk /system/__REMOVED/app/.
# Not exsists on xperia z2 tablet in current firmware: /system/app/SmartcardService
# /system/app/TetherEntitlementCheck
# May help start SIM card pin earlier (?)
# /system/app/StartupFlagV2

# Rich content services ? We can keep them, may be used by SMS/MMS
# /system/app/rcs-startup
# /system/priv-app/RcsVideoShare
# /system/priv-app/rcs-core
# /system/priv-app/rcs-settings

# Low level GSM/network stuff
# Works without these, but do not change battery usage, so let's keep them
# /system/priv-app/CNEService
# /system/priv-app/QtiTetherService
# /system/priv-app/fast-dormancy-wakeup
# /system/priv-app/qcrilmsgtunnel


###############################################
# 20150517 : boots and works OK without the following ones,
# but some features are obviously lost

# Bluetooth & NFC
# /system/app/Bluetooth
# /system/app/NfcNci
# /system/app/BluetoothMidiService
# /system/app/AptxNotifier

# Camera
# /system/app/CameraAddonPermission-release
# /system/app/CameraCommonPermission-release
# /system/app/CameraExtensionPermission-release
# /system/priv-app/CameraCommon
# /system/priv-app/SemcCameraUI-generic-xhdpi-release
# /system/priv-app/SuperVideoCamera-xhdpi-release

# Wifi (CaptivePortal = free wifi access points with intermediate web page ?)
# /system/app/CaptivePortalLogin
# /system/app/HotspotAdvancedSetting
# /system/priv-app/SomcWifiService
# Used to raise power when acting as a hotspot, and decrease it when power is low :
# /system/app/TransmitPower

# Fingerprint (works ok without this Sony one), but :
# java.lang.Exception: The FPC extension service could not be loaded
# /system/priv-app/SomcFingerprint
# /system/app/FingerprintServiceExtension
# Fingerprint "FIDO" stuff, fingerprint unlock works without these two apk
# /system/app/FingerprintASM
# Other FIDO stuff (not in CM13)
# /system/app/FidoClient
# /system/priv-app/FidoCryptoService

# Other Sony stuff we can remove
# /system/app/HeadphoneSelection
# /system/app/UnsupportedHeadsetNotifier
# /system/app/SemcAutoPowerOff
# /system/app/SemcPowerSaveModule
# Cnap = Calling Name Provider
# /system/app/SemcCnapProvider
# Restart if SIM inserted/removed popup
# /system/app/SemcSimDetection
# /system/app/TouchFilterPackageSwitchService
# This one installs packages (?)
# /system/app/package-courier-release

# Sound
# /system/app/SoundEnhancement
# /system/priv-app/MusicFX
# /system/priv-app/SoundRecorder

# Sony images enhancement
# Let's keep it to validate drmrestore
# /system/app/ImageEnhancer
# /system/app/ImageProcessorPermission-release

# Standard stuff (also in CM13)
# /system/app/PacProcessor
# /system/app/Radio3
# /system/app/WAPPushManager
# WapPush is Sony only
# /system/priv-app/WapPush

# /system/app/white-balance
# /system/priv-app/ApnUpdater-release
# /system/priv-app/CallLogBackup
# /system/priv-app/CredentialManagerService
# /system/priv-app/CustomizationSelector-xhdpi-release
# /system/priv-app/CustomizedSettings-release
# Low level security (IMEI, SIM, S1 cert...) :
# /system/priv-app/DeviceSecurityService
# /system/priv-app/ExtendedBootLockService
# /system/priv-app/ScreenRecording
# /system/priv-app/SimlockUnlockApp
# /system/priv-app/SmartSearch
# /system/priv-app/TopContactsProvider
# /system/priv-app/VpnDialogs
# Lots of location services in this Qualcomm app
# /system/priv-app/com.qualcomm.location
# USB stuff, mainly for japanese carrier ? (not many translations in ressources)
# /system/priv-app/enhancedusbux


###############################################
# Google core stuff
# Apps included in Open Gapps PICO :
# ConfigUpdater
# GoogleBackupTransport
# GoogleContactsSyncAdapter
# GoogleFeedback
# GoogleLoginService
# GoogleOneTimeInitializer
# GooglePartnerSetup
# GoogleServicesFramework
# Phonesky
# PrebuiltGmsCore
# SetupWizard
# Stock Google stuff, that we can remove if we want to install Gapps pico instead
mv /system/app/GoogleCalendarSyncAdapter /system/__REMOVED/app/.
mv /system/app/GoogleContactsSyncAdapter /system/__REMOVED/app/.
# /system/priv-app/ConfigUpdater
mv /system/priv-app/GoogleBackupTransport /system/__REMOVED/priv-app/.
mv /system/priv-app/GoogleFeedback /system/__REMOVED/priv-app/.
# /system/priv-app/GoogleLoginService
mv /system/priv-app/GoogleOneTimeInitializer /system/__REMOVED/priv-app/.
# /system/priv-app/GooglePackageInstaller
mv /system/priv-app/GooglePartnerSetup /system/__REMOVED/priv-app/.
# /system/priv-app/GoogleServicesFramework
# /system/priv-app/Phonesky
# /system/priv-app/GmsCore
# /system/priv-app/SetupWizard
# We can remove some of them, but some others of them are needed : otherwise, bootloop.

###############################################
# 20150517 : apk we still keep (probably all essentials)
# /system/app/CertInstaller
# /system/app/ClockWidgets-release
# /system/app/CoreSettings
# /system/app/DocumentsUI
# /system/app/DownloadProviderUi
# /system/app/ExactCalculator
# /system/app/ExternalKeyboardsInternational
# /system/app/KeyChain
# /system/app/LockscreenSettings-release
# /system/app/SemcSettings
# /system/app/ServiceMenu
# /system/app/TimeService
# /system/app/UserDictionaryProvider
# /system/app/WebViewGoogle
# /system/app/app-textInputLive-release
# /system/app/bootinfo
# /system/app/duidgenerator
# /system/app/home-release
# /system/app/telresources
# /system/app/wallpaperpicker-release
# /system/app/xperia-keyboard-dictionaries
# /system/priv-app/CalendarProvider
# /system/priv-app/CarrierConfig
# /system/priv-app/Conversations
# /system/priv-app/DefaultContainerService
# /system/priv-app/DownloadProvider
# /system/priv-app/ExternalStorageProvider
# /system/priv-app/FusedLocation
# /system/priv-app/InCallUI
# /system/priv-app/InputDevices
# /system/priv-app/LocalContacts
# /system/priv-app/ManagedProvisioning
# /system/priv-app/MediaProvider
# /system/priv-app/MmsService
# /system/priv-app/Phonebook
# /system/priv-app/ProxyHandler
# /system/priv-app/SEMCSetupWizard
# /system/priv-app/SemcCalendar
# /system/priv-app/SemcClock
# /system/priv-app/SemcContactsProvider
# /system/priv-app/SemcTelephonyProvider
# /system/priv-app/Settings
# /system/priv-app/SettingsProvider
# /system/priv-app/Shell
# SimCotacts = services with "sdn" in names
# /system/priv-app/SimContacts
# /system/priv-app/StatementService
# /system/priv-app/SystemUI
# /system/priv-app/Tag
# /system/priv-app/TeleService
# /system/priv-app/Telecom
# /system/priv-app/WallpaperCropper
# /system/priv-app/sound-picker-release

# The end.
